//imports
const azure = require('azure-storage');
const TableUtilities = azure.TableUtilities;
const fs = require('fs');
const moment = require('moment');

//constants definitions
const storageAccount = 
    //'rocstorage01';
    "vbgstorage01";
    const tableName = "roomtelemetry";
if (storageAccount == 'rocstorage01') {
    const loginName = 'rocstorage01';
    const loginKey = 'IOJNh2Sq0FBxR6zEsL4Tcs3cb0OjWE2FjPdOen08e7bPNFdnuT60h3TBgNBvHMtCkwBAk+6ts3ZBDi1bd72z0w==';
}

else if (storageAccount == 'vbgstorage01') {
    const loginName = 'vbgstorage01';
    const loginKey = 'u4enZRSUa4sgRPWONVkNFnCjMJhli99dcW2imWjHs4VCNE3t2l1iwvLYcMxL7GrvWKrr4ySCcT7h+Zt70NKppg==';
}

else {
    throw ("Unknown storageAccount");
}

const monthsToDelete = 2;
const dateToRemove = new Date(moment().subtract(monthsToDelete, 'months').calendar());
const backTable = tableName + "Backup";


//imported function definitions
const tableClient = azure.createTableService(loginName, loginKey);
const TableQuery = azure.TableQuery;
const TableBatch = azure.TableBatch;


function findEntitiesDelete(continuationToken, callback) {
    //The query defines what records to find.
    //in this case everything older than dateToRemove
    let query = new TableQuery().where(TableQuery.dateFilter('EventTime', TableUtilities.QueryComparisons.LESS_THAN, dateToRemove));
    queryAnswer = tableClient.queryEntities(tableName, query, continuationToken, function (error, result) {
        let iteratie = 0;

        //if the query gets more than 1000 results, if will only fetch 1000
        //however it will also return a continuationToken where to start from in the next iteration
        continuationToken = result.continuationToken;
        const entries = JSON.parse(JSON.stringify(result.entries));
        queryResults = entries.length;


        //Start for loop. per batch execute once
        for (iteratie = 0; iteratie < entries.length;) {
            console.log("----------------Starting a batch----------------");
            deleteInBatch();
        };

        function deleteInBatch() {
            console.log("Starting deleteInBatch");
            //find multiple entries that match the query and put them in a batch
            //batchSave is for saving to a backup table, batchDelete will delete those same entries
            let batchDelete = new TableBatch();
            let batchSave = new TableBatch();
            for (; iteratie < entries.length;) {
                try {
                    //add entry to batch
                    //and write which entry will be deleted to the log
                    batchDelete.deleteEntity(entries[iteratie]);
                    batchSave.insertEntity(entries[iteratie]);
                    fs.appendFile('log.txt', new Date() + ': Deleting Entry: ' + entries[iteratie]['PartitionKey']['_'] + ', ' + entries[iteratie]['RowKey']['_'] + '\n', (err) => {
                        if (err) throw err;
                    });

                    iteratie++;

                    //azure only supports batch sizes of 100
                    if (batchDelete.size == 100) {
                        console.log("Closing batch, batch is full.")
                        break;
                    }
                }

                //azure only supports batch with a single partition key
                //if another partition key is loaded into the query stop filling batch and execute it
                //then restart after
                catch (error) {
                    if (error.name == "SR.BATCH_ONE_PARTITION_KEY") {
                        console.log("Closing batch, PartitionKey does not match");
                        break;
                    }
                }
            }

            //only execute the batch if it exists
            if (batchDelete.size() > 0) {
                console.log("Executing batch.");
                tableClient.executeBatch(backTable, batchSave, function(error, result) {
                    if (error) {
                        console.log(error);
                        throw error;
                    }
                })
                tableClient.executeBatch(tableName, batchDelete, function (error, result) {
                    if (error) {
                        console.log(error);
                    }
                });
            }
        }

        //restart the function from the continuationToken
        if(continuationToken) {
            console.log("Continueing: " + JSON.stringify(continuationToken));
            return findEntitiesDelete(continuationToken);
        }
        else {
            console.log("Cleaning completed.");
        }
    });
    console.log("Answer: " + queryAnswer);
    //Create a log entry to indicate what table is being cleaned
    fs.appendFile('log.txt', new Date() + ': ----------------Started cleaning ' + tableName + '----------------\n', (err) => {
        if (err) throw err;
    });
    context.done();
}

//start
console.log("Starting with cleanup of table: " + tableName);
findEntitiesDelete();
